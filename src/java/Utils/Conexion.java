/*

 */
package Utils;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author troy
 */
public class Conexion {

    public static Connection getConection(){//no se instancia solo se llama por clase
        Connection cn=null;
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/ecomerce",
                    "root","");  
        }
        catch(Exception e)
        {
            System.out.print("Error en la conexion"+e);
        }
        return cn;
    }
    public static void main(String[] args) {
     Conexion.getConection();
    }
    
}
