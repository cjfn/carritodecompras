/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.DetalleVenta;
import Modelo.Producto;
import Modelo.ProductoDB;
import Modelo.Venta;
import Modelo.VentaDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



/**
 *
 * @author troy
 */
@WebServlet(name = "ServletControlador", urlPatterns = {"/ServletControlador"})
public class ServletControlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
      protected void processRequest(HttpServletRequest request, HttpServletResponse response)//recibe peticiones get y post
            throws ServletException, IOException {
            
            String accion=request.getParameter("accion");
            //valido acciones
            if(accion.equals("AnadirCarrito"))
            {
                this.anadirCarrito(request, response);//CARRITO
            }
            
            else if(accion.equals("RegistrarProducto"))
            {
                this.registrarProducto(request, response);//INSERT
            }
            
            else if(accion.equals("ModificarProducto"))
            {
                this.actualizarProducto(request, response);//UPDATE
            }
        }
      
      private void registrarProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
          //agrego parametros con parse o conversiones
        String pro=request.getParameter("txtNombre");
        double pre=Double.parseDouble(request.getParameter("txtPrecio"));
        String img=request.getParameter("txtImagen");
        
        Producto p=new Producto(pro, pre, img);//creo nuevo objeto en constructor Producto
         boolean rpta = ProductoDB.insertarProducto(p);//variable true/false
        if (rpta) {
            response.sendRedirect("mensaje.jsp?men=Se registro del producto de manera correcta");
        } else {
           response.sendRedirect("mensaje.jsp?men=No se registro el producto");
        }
   }
      
      //ACTUALIZAR
     private void actualizarProducto(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Producto p = new Producto(Integer.parseInt(request.getParameter("txtCodigo")),//creo objeto tipo prodcto
        request.getParameter(("txtNombre").toString()),//nombre
         Double.parseDouble(request.getParameter("txtPrecio")));//precio
        boolean rpta = ProductoDB.actualizarProducto(p);//enviar objeto a db
        if (rpta) {
            response.sendRedirect("mensaje.jsp?men=Se actualizo el producto de manera correcta");
        } else {
            response.sendRedirect("mensaje.jsp?men=No se actualizo el producto");
        } 
        //response.sendRedirect("FormularioPago.jsp?total="+total);
     }
   
    
    private void anadirCarrito(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,IOException{
        
        HttpSession sesion = request.getSession();// variable se sesion 
        ArrayList<DetalleVenta> carrito;//variable array con detalle cantidad de producto
        
        if (sesion.getAttribute("carrito") == null) {//si la cesta esta vacia
            carrito = new ArrayList<DetalleVenta>();//inicio de 0
        } else {
            carrito = (ArrayList<DetalleVenta>) sesion.getAttribute("carrito");//se llena con sesion de carrito
        }
        Producto p = ProductoDB.obtenerProducto(Integer.parseInt(request.getParameter("txtCodigo")));//traigo datos de producto
        
        DetalleVenta d = new DetalleVenta();//creo objeto
        d.setCodigoProducto(Integer.parseInt(request.getParameter("txtCodigo")));//agrego codigo
        d.setProducto(p);//traigo todos los datos
        d.setCantidad(Double.parseDouble(request.getParameter("txtCantidad")));//viene de formulario
        double subTotal = p.getPrecio() * d.getCantidad();//calculo subtotal
        if (subTotal > 50) {//si el subtotal es mayor a 50
            d.setDescuento(subTotal * 0.05);//hago descuento de 5%
        } else {
            d.setDescuento(0);//no hago descuento
        }
        int indice = -1;//no existe
        for (int i = 0; i < carrito.size(); i++) {
            DetalleVenta det = carrito.get(i);
            if (det.getCodigoProducto() == p.getCodigoProducto()) {
                indice = i;// no hay uno igual
                break;
            }
        }
        if (indice == -1) {
            carrito.add(d);//permite añadir productos que no estan repetidos
        }
        sesion.setAttribute("carrito", carrito);
       response.sendRedirect("registrarVenta.jsp");
     
    }
    
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
