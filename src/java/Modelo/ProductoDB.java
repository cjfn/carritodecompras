
package Modelo;

/**
 *
 * @author troy - METODOS DE ACCESO A PRODUCTO
 */

import java.sql.*;
import java.util.ArrayList;
import Utils.Conexion;
public class ProductoDB {
    public static ArrayList<Producto> obtenerProducto()//lista de arreglo con todos los productos
    {
        ArrayList<Producto> lista = new ArrayList<Producto>();
        try
        {
            CallableStatement cl = Conexion.getConection().prepareCall("{call ListarProductos()}");//ejecuto sp
            ResultSet rs =cl.executeQuery();//ejecuto query
            while(rs.next())//mientras exxista algo en la query
            {
                Producto p= new Producto(rs.getInt(1),rs.getString(2), rs.getDouble(3), rs.getString(4));//creacion de objeto con n elementos
                lista.add(p);//agrego a la lista
            }
        }
        catch(Exception e)
        {
            
        }
        return lista;
    }
    
    public static Producto obtenerProducto(int codigo)
    {
        Producto p=null;
        try
        {
            CallableStatement cl= Conexion.getConection().prepareCall("{CALL sp_ProductoCod(?)}");
            cl.setInt(1,codigo);//cadena entera
            ResultSet rs = cl.executeQuery();
            while (rs.next())
            {
                p=new Producto(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4));
            }
        }
        catch(Exception e)
        {
        }
            return p;
                
                
    }
    
     public static boolean insertarProducto(Producto p) {
        boolean rpta = false;//variable con respuesta true false
        try {
            Connection cn = Conexion.getConection();//conexion
            CallableStatement cl = cn.prepareCall("{CALL sp_insertarPro(?,?,?)}"); //ejecuta sp
            cl.setString(1, p.getNombre());//parametros
            cl.setDouble(2, p.getPrecio());
            cl.setString(3, p.getImagen());
            int i=cl.executeUpdate();//ejecuta actualizacion
            if(i==1){//valida respuesta
                rpta=true;
            }else{
                rpta=false;
            }
        } catch (Exception e) { }
        return rpta;
    } 
    
    
    public static boolean actualizarProducto(Producto varproducto)//actualizar
    {
        boolean rpta=false;//retorna valor true false
        try
        {
            Connection cn= Conexion.getConection();//referencia a conexion
            CallableStatement cl = cn.prepareCall("{call sp_actualizarPro(?,?,?)}");//llamar a stored procedure
            cl.setInt(1, varproducto.getCodigoProducto());//establezco parametros
            cl.setString(2,varproducto.getNombre());
            cl.setDouble(3,varproducto.getPrecio());
            int i=cl.executeUpdate();//ejecutar modificacion
            if(i==1)
            {
                rpta=true;
            }
            else
            {
                rpta=false;
            }
            
        }
        catch(Exception e)
        {}
        return rpta;
    }
}
