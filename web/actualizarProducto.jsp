<%-- 
    Document   : actualizarProducto
    Created on : 18-mar-2017, 21:06:23
    Author     : troy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="Modelo.*" %>
<%@page session="true" %>
<% 
    Producto p=ProductoDB.obtenerProducto(Integer.parseInt(request.getParameter("id")));
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
     <table border="0" width="1000" align="center">
            <tr bgcolor="skyblue">
                <th><a href="index.jsp">Catalogo</a></th>
                <th><a href="RegistrarProducto.jsp">Registrar Producto</a></th>
                <th><a href="<registrarVenta.jsp">Registrar Venta</a></th>
                <th><a href="consultarVentas.jsp">Constular Ventas</a></th>
                <th><a href="ServletLogueo?accion=cerrar">Cerrar sesion</a></th>
            </tr>
        </table>
         <h2 align="center">Actualizar Productos</h2>
        <form action="ServletControlador" method="post">
           <table border="0" width="400" align="center">
              <tr>
                   <td>Código: </td>
             <td><input type="text" name="txtCodigo" value="<%= p.getCodigoProducto()%>" readonly></td>
               </tr><tr>
                   <td>Nombre: </td>
                 <td><input type="text" name="txtNombre" value="<%= p.getNombre()%>"> </td>
               </tr><tr>
                   <td>Precio: </td>
                   <td><input type="text"  name="txtPrecio" value="<%= p.getPrecio()%>">       
                   </td>
               </tr><tr>
               <th colspan="2"><input type="submit" value="Actualizar" name="btnActualizar" /></th>
               </tr>
                <input type="hidden" name="accion" value="ModificarProducto" />
        </form> 
    </body>
</html>
