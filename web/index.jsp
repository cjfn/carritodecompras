<%-- 
    Document   : index
    Created on : 16-mar-2017, 21:34:46
    Author     : troy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.*" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <table border="0" width="1000" align="center">
            <tr bgcolor="skyblue">
                <th><a href="index.jsp">Catalogo</a></th>
                <th><a href="RegistrarProducto.jsp">Registrar Producto</a></th>
                <th><a href="<registrarVenta.jsp">Registrar Venta</a></th>
                <th><a href="consultarVentas.jsp">Constular Ventas</a></th>
                <th><a href="ServletLogueo?accion=cerrar">Cerrar sesion</a></th>
            </tr>
        </table>
        <h2 align="center">Catalogo de productos</h2>
        <table border="0" width="1000" align="center">
            <%
                ArrayList<Producto> lista=ProductoDB.obtenerProducto();
                int salto=0;
                for(Producto p:lista)
                {
                    %>
                    <th><img src="img/<%=p.getImagen() %> " widt="140" height="140">
                        <p><%=p.getNombre() %><br/>
                        <%=p.getPrecio() %>
                        </p>
                        <a href="actualizarProducto.jsp?id=<%= p.getCodigoProducto()%>">Modificar</a>
                        <a href="anadirCarrito.jsp?id=<%= p.getCodigoProducto()%>">Añadir</a>
                    </th>
                    <%
                        salto++;
                                if(salto==3){
                                    %>
                                    <tr>
                                        <%
                                            salto=0;

                                }
                        
                }
                %>
        </table>
    </body>
</html>
