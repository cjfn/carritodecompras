<%-- 
    Document   : registrarVenta
    Created on : 18-mar-2017, 10:23:20
    Author     : troy
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.*"%>
<%@page import="Modelo.*" %>
<%@page session="true" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <table border="0" width="1000" align="center">
            <tr bgcolor="skyblue">
                <th><a href="index.jsp">Catalogo</a></th>
                <th><a href="RegistrarProducto.jsp">Registrar Producto</a></th>
                <th><a href="<registrarVenta.jsp">Registrar Venta</a></th>
                <th><a href="consultarVentas.jsp">Constular Ventas</a></th>
                <th><a href="ServletLogueo?accion=cerrar">Cerrar sesion</a></th>
            </tr>
        </table>
        
        <h2 align="center">Carrito de Compras</h2>
        <form method="post" action="ServletControlador">
        <input type="hidden" name="accion" value="RegistrarVenta" />
        <table border="1" align="center" width="450">
    

            <tr style="background-color: skyblue; color: black; font-weight: bold">
                <td width="180">Nombre</td>
                <td>Precio</td><td>Cantidad</td>
                <td>Descuento</td><td>Sub. Total</td>
            </tr>
            <%
            double total=0;//inicio en 0
                ArrayList<DetalleVenta> lista = 
                        (ArrayList<DetalleVenta>)session.getAttribute("carrito");

                if(lista!=null){

                    for (DetalleVenta d : lista) {
            %>
                    <tr>
                        <td><%= d.getProducto().getNombre()%></td>
                        <td><%= d.getProducto().getPrecio()%></td>
                        <td><%= d.getCantidad()%></td>
                        <td><%=String.format("%.2f",d.getDescuento())%></td>
                        <td><%=String.format("%.2f",(d.getProducto().getPrecio() * d.getCantidad())-d.getDescuento())%></td>
                    </tr>
    <%
    
    
    total=total+(d.getProducto().getPrecio() * d.getCantidad())-d.getDescuento();
                    }
                }
    %>
    <tr>
       
        <th colspan="4" align="right">Total</th>
         <th><%=String.format("%.2f",total)%></th>
    </tr>
    <tr >
        <th colspan="5">
            <input type="submit" value="Registrar Venta" name="btnVenta" /></th>
    </tr>
</table>
     <input type="hidden" name="total" value="<%=total%>" />
</form>
</div>
    <h3 align="center"><a href="index.jsp">Seguir Comprando</a> || 
        <a href="ServletLogueo?accion=Cancelar">Cancelar Compra</a>
    
    </h3>
  </div>

    </body>
</html>
